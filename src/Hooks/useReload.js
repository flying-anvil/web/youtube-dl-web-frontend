import {useEffect} from "react";

export default function useReload(onReload: Function, reloadKey: string = 'F5') {
  useEffect(() => {
    const refresh = (event: KeyboardEvent) => {
      if (event.key !== reloadKey || event.ctrlKey === true) {
        return;
      }

      event.preventDefault();
      onReload(event);
    }

    document.body.addEventListener('keydown', refresh);
    return () => document.body.removeEventListener('keydown', refresh);
  }, [onReload, reloadKey])
}
