// @flow
import React, {useEffect, useState} from 'react';
import {Button, Card, Col, Form, Row, Table} from "react-bootstrap";
import {dateToLocale, secondsToHoursMinutesSeconds} from "../../Helper/DateFormatter";
import type {WorkerStateType, WorkersType} from "../../types/Worker";
import {roundPlaces} from "../../Helper/MathHelper";
import useReload from "../../Hooks/useReload";
import * as Api from "../../Helper/ApiRequest";
import WorkerSuccess from "./WorkerSuccess";

type Props = {};

export default function WorkerList(props: Props) {
  const [workers: WorkersType, setWorkers] = useState([]);
  const [requestState: WorkerStateType, setRequestState] = useState('all');

  const reload = () => {
    const abort = new AbortController();

    Api.workers(requestState).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response);
        return;
      }

      setWorkers(response.data.workers);
    })

    return abort;
  };

  const handleStateChange = (event) => {
    const newState = event.target.value;
    setRequestState(newState);
  };

  useEffect(() => {
    const abort = reload();

    return () => {
      abort.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [requestState]);
  useReload(reload);

  return (
    <Col lg={12}>
      <Card>
        <Row>
          <Col>
            <Card.Header as="h4">Download Workers</Card.Header>
          </Col>
          <Col className={'text-right'} md={2}>
            <Card.Header>
              <Form.Control as="select" name="type" onChange={handleStateChange} value={requestState}>
                <option value={'all'}>Alle</option>
                <option value={'running'}>Aktiv</option>
                <option value={'stopped'}>Gestoppt</option>
              </Form.Control>
            </Card.Header>
          </Col>
        </Row>
        <Card.Body>
          <Table responsive="lg" hover striped>
            <thead>
            <tr>
              <th style={{width: '10%'}}>ID</th>
              <th>Start</th>
              <th>Stop</th>
              <th>Erfolg</th>
              <th>Playlist</th>
              <th>Video</th>
              <th>Download start</th>
              <th>Geschw.</th>
              <th>Verbl.</th>
              <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            {
              workers.map((current: WorkerType) => {
                const {workerId, workerState, workerStartDate, workerStopDate, downloadedVideosCount, failedVideosCount,
                  currentPlaylist, currentVideo, currentProgress, downloadStartDate, currentSpeed, eta} = current;

                return <tr key={workerId}>
                  <td><code>{workerId}</code></td>
                  <td>{dateToLocale(workerStartDate)}</td>
                  <td>{dateToLocale(workerStopDate)}</td>
                  <td><WorkerSuccess success={downloadedVideosCount} failed={failedVideosCount}/></td>
                  <td>{currentPlaylist}</td>
                  <td>{currentVideo}</td>
                  <td>{dateToLocale(downloadStartDate)}</td>
                  <td>{currentSpeed && `${roundPlaces(currentSpeed, 2)}/s`}</td>
                  <td className="text-right">{eta && secondsToHoursMinutesSeconds(eta)}</td>
                  <td>
                    <Button variant={'outline-info'} size={'sm'}>Knopf</Button>
                  </td>
                </tr>
              })
            }
            </tbody>
          </Table>
        </Card.Body>
      </Card>
    </Col>
  );
};
