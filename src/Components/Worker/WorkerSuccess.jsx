// @flow
import React from 'react';
import TooltipBadge from "../TooltipBadge";
import {Col, ProgressBar, Row} from "react-bootstrap";

type Props = {
  success: number,
  failed: number,
};

export default function WorkerSuccess(props: Props) {
  const {success, failed} = props;
  const totals = success + failed;

  return (
    <>
      <Row>
        <Col>
          <TooltipBadge name={'worker-success'} variant={'success'} badgeContent={success} tooltipContent={'Erfolgreich heruntergaladen'}/>
          &nbsp;
          <TooltipBadge name={'worker-fail'} variant={'danger'} badgeContent={failed} tooltipContent={'Fehlerhaft heruntergaladen'}/>
        </Col>
      </Row>
      <Row>
        <Col>
          <ProgressBar>
            <ProgressBar variant={"success"} now={success} max={totals}/>
            <ProgressBar variant={"danger"} now={failed} max={totals}/>
          </ProgressBar>
        </Col>
      </Row>
    </>
  );
};
