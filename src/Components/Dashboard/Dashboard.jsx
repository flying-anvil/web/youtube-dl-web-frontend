// @flow
import React from 'react';
import {Col, Row} from "react-bootstrap";
import PlaylistStatistics from "./Statistics/PlaylistStatistics";
import VideoStatistics from "./Statistics/VideoStatistics";
import WorkerStatistics from "./Statistics/WorkerStatistics";
import ScanStatistics from "./Statistics/ScanStatistics";

type Props = {};

export default function Dashboard(props: Props) {
  return (
    <Col md={12}>
      <Row className={'mb-5'}>
        <Col md={6}>
          <PlaylistStatistics/>
        </Col>

        <Col md={6}>
          <VideoStatistics/>
        </Col>
      </Row>

      <Row className={'mb-5'}>
        <Col md={6}>
          <WorkerStatistics/>
        </Col>

        <Col md={6}>
          <ScanStatistics/>
        </Col>
      </Row>
    </Col>
  );
};
