// @flow
import React, {useEffect, useState} from 'react';
import {Card} from "react-bootstrap";
import StatisticsSkeleton from "./StatisticsSkeleton";
import type {WorkersType} from "../../../types/Worker";
import * as Api from "../../../Helper/ApiRequest";
import useReload from "../../../Hooks/useReload";
import {roundPlaces} from "../../../Helper/MathHelper";
import TooltipBadge from "../../TooltipBadge";

type Props = {};

export default function WorkerStatistics(props: Props) {
  const [workers: WorkersType, setWorkers] = useState([]);

  const reload = () => {
    const abort = new AbortController();

    Api.workers('running', abort).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response);
        return;
      }

      setWorkers(response.data.workers);
    })

    return abort;
  };

  useEffect(() => {
    const abort = reload();

    return () => {
      abort.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useReload(reload);

  const reduceActiveWorker = () => {
    if (workers.length === 0) return 0;

    return workers
      .map((current: WorkerType) => {
        return current.currentVideo === null ? 0 : 1;
      })
      .reduce((accumulator: number, current: number) => accumulator + current);
  }

  const reduceDownloadSpeed = () => {
    if (workers.length === 0) return 0;

    return workers
      .map((current: WorkerType) => {
        return current.currentSpeed || 0;
      })
      .reduce((accumulator: number, current: WorkerType) => accumulator + (current.currentSpeed || 0));
  }

  return (
    <StatisticsSkeleton title="Worker Statistiken">
      <ul>
        <li>
          <span>Worker:</span>
          &nbsp;
          <span style={{cursor: 'default'}}>
            <TooltipBadge name="worker-total" variant="info" badgeContent={workers.length} tooltipContent={'Laufende Worker'}/>
            &nbsp;
            <TooltipBadge name="worker-active" variant="success" badgeContent={reduceActiveWorker()} tooltipContent={'Aktive Worker'}/>
            &nbsp;
            <TooltipBadge name="worker-inactive" variant="secondary" badgeContent={workers.length - reduceActiveWorker()} tooltipContent={'Inaktive Worker'}/>
          </span>
        </li>
        <li>
          Aktuelle Downloadgeschwindigkeit: {roundPlaces(reduceDownloadSpeed(), 2)} MiB/s
        </li>
      </ul>
    </StatisticsSkeleton>
  );
};
