// @flow
import React from 'react';
import {Card} from "react-bootstrap";

type Props = {
  title: string,
  children: ?any,
};

export default function StatisticsSkeleton(props: Props) {
  const {title, children} = props;

  return (
    <Card>
      <Card.Header as={'h5'}>{title}</Card.Header>
      <Card.Body>
        {children}
      </Card.Body>
    </Card>
  );
};
