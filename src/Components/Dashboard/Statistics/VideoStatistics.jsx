// @flow
import React from 'react';
import {Card} from "react-bootstrap";
import StatisticsSkeleton from "./StatisticsSkeleton";

type Props = {};

export default function VideoStatistics(props: Props) {
  return (
    <StatisticsSkeleton title="Video Statistiken">
      <Card.Text>
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
        sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
        sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
        Stet clita kasd gubergren, no sea takimata sanctus est.
      </Card.Text>
    </StatisticsSkeleton>
  );
};
