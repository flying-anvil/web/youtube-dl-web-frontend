// @flow
import React from 'react';
import type {ScanType} from "../../../../types/Scan";
import TooltipBadge from "../../../TooltipBadge";

type Props = {
  scan: ScanType,
};

export default function LatestScanStatistic(props: Props) {
  const {scan} = props;
  const {id, playlistCount, foundCount, enqueuedCount, duplicateCount, scanDate} = scan;

  return (
    <ul>
      <li>Playlists: {playlistCount}</li>
      <li>
        <span>Videos:</span>
        &nbsp;
        <span>
          <TooltipBadge name={'scan-latest-found'} variant='info' badgeContent={foundCount} tooltipContent='Gefunden'/>
          &nbsp;
          <TooltipBadge name={'scan-latest-enqueued'} variant='success' badgeContent={enqueuedCount} tooltipContent='Neu eingereiht'/>
          &nbsp;
          <TooltipBadge name={'scan-latest-cuplicate'} variant='secondary' badgeContent={duplicateCount} tooltipContent='Bereits eingereiht'/>
        </span>
      </li>
    </ul>
  );
};
