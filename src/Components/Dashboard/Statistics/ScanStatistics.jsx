// @flow
import React, {useEffect, useState} from 'react';
import StatisticsSkeleton from "./StatisticsSkeleton";
import * as Api from "../../../Helper/ApiRequest";
import useReload from "../../../Hooks/useReload";
import {dateToLocale} from "../../../Helper/DateFormatter";
import type {ScanType} from "../../../types/Scan";
import LatestScanStatistic from "./Scan/LatestScanStatistic";

type Props = {};

export default function ScanStatistics(props: Props) {
  const [scan: ScanType, setScan] = useState(null);

  const reload = () => {
    const abort = new AbortController();

    Api.latestScan(abort).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response);
        return;
      }

      setScan(response.data.scan);
    })

    return abort;
  };

  useEffect(() => {
    const abort = reload();

    return () => {
      abort.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useReload(reload);

  return (
    <StatisticsSkeleton title="Scan Statistiken">
      <ul>
        <li>
          <span>Letzter Scan:</span>
          &nbsp;
          <span>{scan && dateToLocale(scan.scanDate) || 'niemals'}</span>
        </li>
        {scan && <LatestScanStatistic scan={scan}/>}
      </ul>
    </StatisticsSkeleton>
  );
};
