// @flow
import React from 'react';
import {Badge, OverlayTrigger, Tooltip} from "react-bootstrap";
import {Variant} from "react-bootstrap/types";

type Props = {
  name: string,
  variant: Variant,
  badgeContent: string,
  tooltipContent: string|any,
};

const style = {cursor: 'default'};

export default function TooltipBadge(props: Props) {
  const {name, variant, badgeContent, tooltipContent} = props;

  return (
    <OverlayTrigger key={name} placement="bottom" overlay={
      <Tooltip style={style} id={`tooltip-${name}`}>{tooltipContent}</Tooltip>
    }>
      <Badge style={style} variant={variant}>{badgeContent}</Badge>
    </OverlayTrigger>
  );
};
