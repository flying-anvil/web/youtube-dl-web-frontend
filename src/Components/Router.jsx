// @flow
import React from 'react';
import Navigation from "./Navigation/Navigation";
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import PlaylistOverview from "./Playlist/Overview/PlaylistOverview";
import PlaylistInfo from "./Playlist/Info/PlaylistInfo";
import WorkerList from "./Worker/WorkerList";
import Dashboard from "./Dashboard/Dashboard";

type Props = {};

const Router = (props: Props) => {
  return (
    <BrowserRouter>
      <Navigation/>

      <Switch>
        <Route exact path="/">
          <Dashboard/>
        </Route>

        <Route exact path="/playlists">
          <PlaylistOverview/>
        </Route>
        <Route exact path="/playlists/:id">
          <PlaylistInfo/>
        </Route>

        <Route exact path="/workers">
          <WorkerList/>
        </Route>

        <Route exact path="/users">
          users
        </Route>

        <Route>
          404 :(
        </Route>
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
