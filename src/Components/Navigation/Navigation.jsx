// @flow
import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavItem from "./NavItem";

type Props = {};

export default function Navigation(props: Props) {
  return (
    <Navbar>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Link to={'/'} className="navbar-brand">YouTube-DL Web</Link>

        <Nav className="mr-auto">
          <NavItem to={'/playlists'} text={'Playlists'}/>
          <NavItem to={'/workers'} text={'Workers'}/>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};
