// @flow
import React, {useEffect, useState} from 'react';
import {Button, Card, OverlayTrigger, ProgressBar, Table, Tooltip} from "react-bootstrap";
import type {PlaclistVideoStatus, RegisteredPlaylistType} from "../../../types/Playlist";
import * as Api from '../../../Helper/ApiRequest'
import useReload from "../../../Hooks/useReload";
import {dateToLocale, secondsToHoursMinutesSeconds} from "../../../Helper/DateFormatter";
import {bytesToHuman} from "../../../Helper/SizeFormatter";
import {BsDownload} from "react-icons/all";
import {roundPlaces} from "../../../Helper/MathHelper";

type Props = {
  playlistId: string,
};

export default function PlaylistVideoList(props: Props) {
  const {playlistId} = props;

  const [playlistVideos: Array<PlaclistVideoStatus>, setPlaylistVideos] = useState([]);
  const [playlistInfo: RegisteredPlaylistType, setPlaylistInfo] = useState({});

  useEffect(() => {
    const abort = new AbortController();

    Api.playlistInfo(playlistId, abort).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response);
        return
      }

      setPlaylistInfo(response.data.playlist);
    });

    return () => {
      abort.abort();
    }
  }, [playlistId]);

  const reload = () => {
    const abort = new AbortController();

    Api.playlistStatus(playlistId).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response);
        return;
      }

      setPlaylistVideos(response.data.videos);
    })

    return abort;
  }

  useEffect(() => {
    const abort = reload();

    return () => {
      abort.abort();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useReload(reload);

  return (
    <Card>
      <Card.Header as="h4">Videos der Playlists <code>{playlistInfo.playlistTitle}</code></Card.Header>
      <Card.Body>
        <Table responsive="lg" hover striped>
          <thead>
          <tr>
            <th>ID</th>
            <th style={{width: '20%'}}>Name</th>
            <th>Uploader</th>
            <th>Gefunden</th>
            <th>Gestartet</th>
            <th>Abgeschlossen</th>
            <th>Größe</th>
            <th style={{width: '10%'}}>Status</th>
            <th>Geschw.</th>
            <th>Verbl.</th>
            <th>Aktion</th>
          </tr>
          </thead>
          <tbody>
          {
            playlistVideos.map((current: PlaclistVideoStatus) => {
              const {videoId, title, foundDate, videoUploader, downloadStatistic} = current;
              const {progress, fileSize, downloadStartDate, downloadFinishedDate, downloadSpeed, eta} = downloadStatistic;

              return <tr key={videoId}>
                <td><code>{videoId}</code></td>
                <td>{title}</td>
                <td>{videoUploader}</td>
                <td>{dateToLocale(foundDate)}</td>
                <td>{dateToLocale(downloadStartDate)}</td>
                <td>{dateToLocale(downloadFinishedDate)}</td>
                <td>{bytesToHuman(fileSize)}</td>
                <td>
                  <ProgressBar variant={'success'} now={progress * 100}/>
                </td>
                <td>{downloadSpeed && `${roundPlaces(downloadSpeed, 2)} MiB/s`}</td>
                <td className="text-right">{secondsToHoursMinutesSeconds(eta)}</td>
                <td>
                  <OverlayTrigger key={`${playlistId}-total`} placement="bottom" overlay={
                    <Tooltip style={{cursor: 'default'}} id={`tooltip-download`}>Herunterladen</Tooltip>
                  }>
                    <Button variant="outline-info" size={'sm'} className="mr-2"><BsDownload/></Button>
                  </OverlayTrigger>

                  <Button variant="outline-warning" size={'sm'} className="mr-2">Knopf 2</Button>
                </td>
              </tr>
            })
          }
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
};
