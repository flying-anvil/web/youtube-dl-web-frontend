// @flow
import React from 'react';
import {Button, Card} from "react-bootstrap";

type Props = {};

export default function PlaylistVideoActions(props: Props) {
  return (
    <Card>
      <Card.Header as="h4">Aktionen</Card.Header>
      <Card.Body>
        <Button variant={"primary"} onClick={() => {}}>Aktion</Button>
      </Card.Body>
    </Card>
  );
};
