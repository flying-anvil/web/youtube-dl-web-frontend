// @flow
import React from 'react';
import {useParams} from "react-router-dom";
import {Col} from "react-bootstrap";
import PlaylistVideoList from "./PlaylistVideoList";
import PlaylistVideoActions from "./PlaylistVideoActions";

type Props = {};

export default function PlaylistInfo(props: Props) {
  const {id} = useParams();

  return (
    <Col lg={12}>
      <PlaylistVideoList playlistId={id}/>
      <div className="mb-4"/>
      <PlaylistVideoActions/>
    </Col>
  );
};
