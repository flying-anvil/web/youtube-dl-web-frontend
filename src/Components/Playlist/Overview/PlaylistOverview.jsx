// @flow
import React from 'react';
import PlaylistAdd from "./PlaylistAdd";
import {Col} from "react-bootstrap";
import PlaylistTable from "./PlaylistTable";

type Props = {};

export default function PlaylistOverview(props: Props) {
  return (
    <Col lg={12}>
      <PlaylistTable/>
      <div className="mb-4"/>
      <PlaylistAdd/>
    </Col>
  );
};
