// @flow
import React from 'react';
import type {PlaylistInfoResponseType} from "../../../types/Playlist";
import {Col, Row, Spinner} from "react-bootstrap";

type Props = {
  isPending: boolean,
  playlistInfo: PlaylistInfoResponseType,
};

export default function PlaylistInfoSummary(props: Props) {
  const {isPending, playlistInfo} = props;

  if (isPending) {
    return <Spinner size={'sm'} animation={'border'}/>;
  }

  if (!playlistInfo) {
    return null;
  }

  return (
    <Row>
      <Col xs={4}>
        Name: {playlistInfo.playlistName}
      </Col>
      <Col xs={4}>
        Anzahl Titel: {playlistInfo.playlistEntries.length}
      </Col>
    </Row>
  );
};
