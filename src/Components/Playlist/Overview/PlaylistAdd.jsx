// @flow
import React, {useState} from 'react';
import {Button, Card, Form, Modal, Spinner} from "react-bootstrap";
import * as Api from "../../../Helper/ApiRequest.js";
import PlaylistInfoSummary from "./PlaylistInfoSummary";
import type {PlaylistInfoResponseType} from "../../../types/Playlist";

type Props = {};

let abort = new AbortController();

export default function PlaylistAdd(props: Props) {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const [playlistInfoPending, setPlaylistInfoPending] = useState(false);
  const [playlistInfo: PlaylistInfoResponseType, setPlaylistInfo] = useState(null);
  const [playlistInfoError, setPlaylistInfoError] = useState(null);

  const [playlistAddPending, setPlaylistAddPending] = useState(false);
  const [playlistAddError, setPlaylistAddError] = useState(null);
  const [playlistAddSuccess, setPlaylistAddSuccess] = useState(null);

  const reset = () => {
    setPlaylistInfoPending(false);
    setPlaylistInfo(null);
    setPlaylistInfoError(null);

    setPlaylistAddPending(false);
    setPlaylistAddError(null);
    setPlaylistAddSuccess(null);
  }

  const handleShow = () => {
    setIsModalOpen(true);
  }

  const doClose = () => {
    reset();

    setIsModalOpen(false);
  }

  const handleIdChange = (event) => {
    const id = event.target.value;

    if (playlistInfoPending) {
        abort.abort();
        setPlaylistInfoPending(true);

        abort = new AbortController();
    }

    if (id.trim().length === 34) {
      if (!playlistInfoPending) {
        setPlaylistInfoPending(true);
        setPlaylistInfoError(null);

        Api.playlistInfoInfo(id, abort).then((response) => {
          setPlaylistInfoPending(false);

          if (response.statusCode !== 200) {
            setPlaylistInfoError(response.data.error)

            return;
          }

          const data: PlaylistInfoResponseType = response.data;
          setPlaylistInfo(data);
        });
      }

      return;
    }

    reset();
  }

  const textDecoration = 'text-muted';

  const handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    event.stopPropagation();

    setPlaylistAddPending(true);

    const data = {};

    for (const input of event.target) {
      const {name, value} = input;

      if (name === '') {
        continue;
      }

      data[name] = value;
    }

    Api.registerPlaylist(data).then((response) => {
      setPlaylistAddPending(false);

      if (response.statusCode !== 200) {
        setPlaylistAddError(response.data.error)

        return;
      }

      console.log(response.data.message);
      setPlaylistAddSuccess(response.data.message);
    });
  }

  return (
    <Card>
      <Card.Header as="h4">Playlist Verwaltung</Card.Header>
      <Card.Body>
        <Button variant={"primary"} onClick={handleShow}>Neue Playlist registrieren</Button>

        <Modal show={isModalOpen} onHide={doClose} size={'lg'}>
          <Modal.Header closeButton>
            <Modal.Title className={textDecoration}>Neue Playlist registrieren</Modal.Title>
          </Modal.Header>

          <Form onSubmit={handleSubmit}>
            <Modal.Body>
              <Form.Group controlId={'playlistId'}>
                <Form.Label className={textDecoration}>Playlist ID</Form.Label>
                <Form.Control
                  type="text"
                  placeholder={'Playlist ID'}
                  name="playlistId"
                  onChange={handleIdChange}
                  disabled={playlistAddPending || playlistAddSuccess}
                />
                <Form.Text className={textDecoration}>
                  <PlaylistInfoSummary isPending={playlistInfoPending} playlistInfo={playlistInfo}/>
                </Form.Text>
                <Form.Text className={'text-danger'}>{playlistInfoError}</Form.Text>
                <Form.Text className={'text-danger'}>{playlistAddError}</Form.Text>
              </Form.Group>

              <Form.Group controlId={'type'}>
                <Form.Label className={textDecoration}>Typ</Form.Label>
                <Form.Control as="select" name="type" disabled={playlistAddPending || playlistAddSuccess}>
                  <option defaultChecked>Audio</option>
                  <option disabled>Video</option>
                </Form.Control>
              </Form.Group>

              <Form.Group controlId={'user'}>
                <Form.Label className={textDecoration}>Nutzer</Form.Label>
                <Form.Control type={'text'} defaultValue={'Anonym'} name="user" disabled={playlistAddPending || playlistAddSuccess}/>
              </Form.Group>

              {/*<hr/>*/}
              {/*<PlaylistInfoSummary isPending={playlistInfoPending} infoSummary={playlistInfoInfo}/>*/}
            </Modal.Body>

            <Modal.Footer>
              <Form.Text className={'text-success'}>{playlistAddSuccess}</Form.Text>
              <Button variant={'success'} type={'submit'} disabled={playlistAddPending || playlistAddSuccess}>
                Registrieren
                {playlistAddPending && <Spinner className={'ml-1'} size={'sm'} animation={'border'}/>}
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </Card.Body>
    </Card>
  );
};
