// @flow
import React from 'react';
import {Badge, OverlayTrigger, Tooltip} from "react-bootstrap";
import type {PlaylistStatusCountsType} from "../../../types/Playlist";

type Props = {
  playlistId: string,
  statusCounts: PlaylistStatusCountsType,
};

export default function PlaylistStatusCounts(props: Props) {
  const {playlistId, statusCounts} = props;
  const {videoCount, downloadCount, pendingCount, failedCount} = statusCounts;
  const style = {cursor: 'default'};

  return (
    <div>
      <OverlayTrigger key={`${playlistId}-total`} placement="bottom" overlay={
        <Tooltip style={style} id={`tooltip-${playlistId}-count`}>Gesamtanzahl</Tooltip>
      }>
        <Badge variant={"primary"}>{videoCount}</Badge>
      </OverlayTrigger>
      &nbsp;
      <OverlayTrigger key={`${playlistId}-downloaded`} placement="bottom" overlay={
        <Tooltip style={style}  id={`tooltip-${playlistId}-count`}>Heruntergeladen</Tooltip>
      }>
        <Badge variant={"success"}>{downloadCount}</Badge>
      </OverlayTrigger>

      &nbsp;
      <OverlayTrigger key={`${playlistId}-pending`} placement="bottom" overlay={
        <Tooltip style={style}  id={`tooltip-${playlistId}-count`}>Aussetehend</Tooltip>
      }>
        <Badge variant={"secondary"}>{pendingCount}</Badge>
      </OverlayTrigger>

      &nbsp;
      <OverlayTrigger key={`${playlistId}-failed`} placement="bottom" overlay={
        <Tooltip style={style}  id={`tooltip-${playlistId}-count`}>Fehlgeschlagen</Tooltip>
      }>
        <Badge variant={"danger"}>{failedCount}</Badge>
      </OverlayTrigger>
    </div>
  );
};
