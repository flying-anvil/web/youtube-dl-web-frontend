// @flow
import React, {useEffect, useState} from 'react';
import {Button, Card, Table} from "react-bootstrap";
import type {RegisteredPlaylistType} from "../../../types/Playlist";
import {Link, useRouteMatch} from "react-router-dom";
import * as Api from "../../../Helper/ApiRequest.js";
import useReload from "../../../Hooks/useReload";
import PlaylistStatus from "./PlaylistStatus";
import {dateToLocale, secondsToHoursMinutesSeconds} from "../../../Helper/DateFormatter";

type Props = {};

export default function PlaylistTable(props: Props) {
  // const {path, url} = useRouteMatch();
  const {path} = useRouteMatch();
  const [playlists, setPlaylists] = useState([]);

  function reload() {
    const abort = new AbortController();

    Api.playlistList(abort).then((response) => {
      if (response.statusCode !== 200) {
        console.error(response.data);
        return;
      }

      setPlaylists(response.data.playlists);
    });

    return abort;
  }

  useEffect(() => {
    const abort = reload();

    return () => {
      abort.abort();
    };
  }, []);

  useReload(reload);

  return (
    <Card>
      <Card.Header as="h4">Registrierte Playlists</Card.Header>
      <Card.Body>
        <Table responsive="lg" hover striped>
          <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Dauer</th>
            <th>Typ</th>
            <th>Registriert am</th>
            <th>Registriert von</th>
            <th style={{width: '25%'}}>Status</th>
            <th>Aktion</th>
          </tr>
          </thead>
          <tbody>
          {
            playlists.map((current: RegisteredPlaylistType) => {
              const {playlistId, playlistTitle, targetType, addedDate, addedBy, statusCounts, duration} = current;

              return <tr key={playlistId}>
                <td><code>{playlistId}</code></td>
                <td>{playlistTitle}</td>
                <td className="text-right">{secondsToHoursMinutesSeconds(duration)}</td>
                <td>{targetType}</td>
                <td>{dateToLocale(addedDate)}</td>
                <td>{addedBy}</td>
                <td style={{cursor: 'default'}}>
                    <PlaylistStatus playlistId={playlistId} statusCounts={statusCounts}/>
                </td>
                <td>
                  <Link to={`${path}/${playlistId}`}><Button variant="outline-info" size={'sm'} className="mr-2">Info</Button></Link>
                  <Button variant="outline-warning" size={'sm'} className="mr-2">Bearbeiten</Button>
                </td>
              </tr>
            })
          }
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  );
};
