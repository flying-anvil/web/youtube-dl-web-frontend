// @flow
import React from 'react';
import PlaylistStatusCounts from "./PlaylistStatusCounts";
import {Col, Row} from "react-bootstrap";
import PlaylistStatusProgress from "./PlaylistStatusProgress";

type Props = {
  playlistId: string,
  statusCounts: PlaylistStatusCounts,
};

export default function PlaylistStatus(props: Props) {
  const {playlistId, statusCounts} = props;

  return (
    <>
      <Row>
        <Col>
          <PlaylistStatusCounts playlistId={playlistId} statusCounts={statusCounts}/>
        </Col>
      </Row>
      <Row>
        <Col>
          <PlaylistStatusProgress statusCounts={statusCounts}/>
        </Col>
      </Row>
    </>
  );
};
