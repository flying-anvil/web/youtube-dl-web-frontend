// @flow
import React from 'react';
import PlaylistStatusCounts from "./PlaylistStatusCounts";
import {ProgressBar} from "react-bootstrap";

type Props = {
  statusCounts: PlaylistStatusCounts,
};

export default function PlaylistStatusProgress(props: Props) {
  const {statusCounts} = props;
  const {videoCount, downloadCount, pendingCount, failedCount} = statusCounts;

  return (
    <ProgressBar>
      <ProgressBar variant={"success"} now={downloadCount} max={videoCount}/>
      <ProgressBar variant={"secondary"} now={pendingCount} max={videoCount}/>
      <ProgressBar variant={"danger"} now={failedCount} max={videoCount}/>
    </ProgressBar>
  );
};
