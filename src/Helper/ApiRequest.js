// @flow

import type {WorkerStateType} from "../types/Worker";

const baseUrl = `${process.env.REACT_APP_API_HOST}/api`;

type RegisterPlaylistParams = {
  playlistId: string,
  type: string,
  user: string,
}

export function playlistList(abortController: ?AbortController): Promise {
  return doRequest('GET', 'playlist', null, abortController);
}

export function playlistInfo(playlistId: string, abortController: ?AbortController): Promise {
  return doRequest('GET', `playlist/${playlistId}`, null, abortController);
}

export function playlistStatus(playlistId: string, abortController: ?AbortController): Promise {
  return doRequest('GET', `playlist/${playlistId}/status`, null, abortController);
}

export function playlistInfoInfo(playlistId: string, abortController: ?AbortController): Promise {
  const route = `playlist/info?playlist=${playlistId}`
  return doRequest('GET', route, null, abortController);
}

export function registerPlaylist({playlistId, type, user}: RegisterPlaylistParams): Promise {
  const body = {
    playlist: playlistId,
    target: type,
    user,
  };

  return doRequest('POST', 'playlist', body);
}

export function workers(requestState: WorkerStateType = 'all', abortController: ?AbortController) {
  return doRequest('GET', `worker?state=${requestState}`, null, abortController);
}

export function latestScan(abortController: ?AbortController) {
  return doRequest('GET', 'scan/latest', null, abortController);
}

function doRequest(method: string, route: string, body: ?Object, abortController: AbortController): Promise {
  const options = {
    method: method,
    headers: {
      'Content-Type': 'application/json',
    },
  }

  if (body) {
    options.body = JSON.stringify(body);
  }

  if (abortController) {
    options.signal = abortController.signal;
  }

  return new Promise((resolve: Function, reject: Function) => {
    fetch(`${baseUrl}/${route}`, options)
      .then((response: Response) => {
        const contentType = response.headers.get('Content-Type');

        if (contentType === 'application/json') {
          response.json().then((json) => {
            resolve({
              statusCode: response.status,
              statusText: response.statusText,
              data: json,
            });
          })

          return;
        }

        resolve({
          statusCode: response.status,
          statusText: response.statusText,
          data: response.body,
        });
      }).catch((error) => {
        if (abortController && abortController.signal.aborted) {
          console.log('aborted');
          return;
        }

        if (!error.headers) {
          console.error(error);
          return;
        }

        const contentType = error.headers.get('Content-Type');

        if (contentType === 'application/json') {
          error.json().then((json) => {
            resolve({
              statusCode: error.status,
              statusText: error.statusText,
              data: json,
            });
          })

          return;
        }

        resolve({
          statusCode: error.status,
          statusText: error.statusText,
          data: body,
        });
    });
  });
}
