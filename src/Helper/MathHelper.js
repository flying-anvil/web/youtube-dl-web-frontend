// @flow

export function roundPlaces(value: number, decimalPlaces: number = 2) {
  const reducer = Math.pow(10, decimalPlaces);

  return Math.round(value * reducer) / reducer;
}
