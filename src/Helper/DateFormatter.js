// @flow

export function dateToLocale(rawDate: ?string) {
  if (!rawDate) {
    return null;
  }

  const date = new Date(rawDate);
  return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`
}

export function secondsToHoursMinutesSeconds(seconds: ?number) {
  if (seconds === null) {
    return null;
  }

  const hours   = parseInt(seconds / 3600);
  const minutes = parseInt((seconds % 3600) / 60);
  const reainingSeconds = seconds % 60;

  const formattedMinutes = minutes.toString().padStart(2, 0);
  const formattedSeconds = reainingSeconds.toString().padStart(2, 0);

  return `${hours}:${formattedMinutes}:${formattedSeconds}`;
}
