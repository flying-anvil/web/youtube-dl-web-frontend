// @flow

import {roundPlaces} from "./MathHelper";

const siPrefixes = {
   '0': '',
   '1': 'k',
   '2': 'M',
   '3': 'G',
   '4': 'T',
   '5': 'P',
   '6': 'E',
   '7': 'Z',
   '8': 'Y',
}

export function bytesToHuman(bytes: number) {
  const prefixIndex = parseInt(logBase(bytes, 1024)) || 0;
  const value       = bytes / (1024 ** prefixIndex);

  if (prefixIndex === 0) {
    return `${bytes} B`;
  }

  const prefix = siPrefixes[prefixIndex.toString()];
  return `${roundPlaces(value, 2)} ${prefix}iB`;
}

function logBase(x: number, base: number) {
  return Math.log(x) / Math.log(base);
}
