// @flow

import * as Api from "./ApiRequest";
import type {PlaylistInfoResponseType} from "../types/Playlist";
import {useState} from "react";

export default function useApiRequest(apiFunction: Function, setSuccessData: Function) {
  const [requestPending, setRequestPendeing] = useState(false);
  const [playlistInfoError, setPlaylistInfoError] = useState(null);

  if (!requestPending) {
    setRequestPendeing(true);
    setPlaylistInfoError(null);

    Api.playlistInfoInfo(id).then((response) => {
      setRequestPendeing(false);

      // if (playlistInfoAbort) {
      //   setPlaylistInfoAbort(false);
      //   return;
      // }
      //
      // setPlaylistInfoAbort(false);

      if (response.statusCode !== 200) {
        setPlaylistInfoError(response.data.error)

        return;
      }

      const data: PlaylistInfoResponseType = response.data;
      setSuccessData(data);
    });
  }
}
