// @flow

export type WorkerStateType = 'all' | 'running' | 'stopped';

export type WorkerType = {
  workerId: string,
  workerState: WorkerStateType,
  workerStartDate: string,
  workerStopDate: string,
  downloadedVideosCount: number,
  failedVideosCount: number,
  currentPlaylist: ?string,
  currentVideo: ?string,
  currentProgress: ?number,
  downloadStartDate: ?string,
  currentSpeed: ?number,
  eta: ?number,
}

export type WorkersType = Array<WorkerType>

