// @flow

export type ScanType = {
  id: string,
  playlistCount: number,
  foundCount: number,
  enqueuedCount: number,
  duplicateCount: number,
  scanDate: string,
}
