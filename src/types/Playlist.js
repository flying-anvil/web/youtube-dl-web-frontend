// @flow

export type PlaylistStatusCountsType = {
  downloadCount: number,
  failedCount: number,
  pendingCount: number,
  videoCount: number,
}

export type RegisteredPlaylistType = {
  playlistId: string,
  playlistTitle: string,
  targetType: 'audio'|'video',
  addedDate: string,
  addedBy: string,
  statusCounts: PlaylistStatusCountsType,
  duration: number,
};

export type PlaylistInfoSummaryEntryType = {
  id: string,
  title: string,
  uploader: string,
}

export type PlaylistInfoResponseType = {
  playlistId: string,
  playlistName: string,
  playlistEntries: Array<PlaylistInfoSummaryEntryType>,
};

export type VideoDownloadStatisticType = {
  progress: number,
  fileSize: number,
  downloadStartDate: ?string,
  downloadFinishedDate: ?string,
  downloadSpeed: number,
  eta: ?number,
}

export type PlaclistVideoStatus = {
  videoId: string,
  title: string,
  foundDate: string,
  videoUploader: string,
  downloadStatistic:VideoDownloadStatisticType,
}
